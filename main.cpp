#include <QtGui/QGuiApplication>
#include <QQmlContext>
#include "qtquick2applicationviewer.h"

#include <QQmlEngine>

#include "Skin.h"

int main(int argc, char *argv[])
{
    QGuiApplication app(argc, argv);

    QtQuick2ApplicationViewer viewer;
    qmlRegisterType<Skin>("Cpp", 1, 0, "Skin");
    viewer.rootContext()->setContextProperty("pwd", QGuiApplication::applicationDirPath());
    viewer.setSource(QUrl("qrc:/qml/gamejam.qml"));
    viewer.showExpanded();

    return app.exec();
}
