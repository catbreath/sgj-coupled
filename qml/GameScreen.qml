import QtQuick 2.0
import QtGraphicalEffects 1.0
import QtMultimedia 5.0
import Cpp 1.0

Rectangle {
    id: gameScreen
    color: "darkgreen"

    signal exitToMenuClicked

    property bool debug: false

    property bool gameOver: false
    property string gameOverCause

    property date startTime: new Date()

    Audio {
        id: gameOverMusic
        source: pwd + "/audio/You died stupid.wav"
        volume: 0.45
    }

    Audio {
        id: ripSound
        source: pwd + "/audio/Ripped apart.wav"
        onStopped: {
            gameOverMusic.play();
        }
    }

    function move(player, toPos) {
        var moved = false;
        var otherPlayer = player === player1 ? player2 : player1;
        if (skin.distance(toPos.x, toPos.y, player.width, player.height, otherPlayer.x, otherPlayer.y, otherPlayer.width, otherPlayer.height) < skin.distanceThreshold) {
            player.x = toPos.x;
            player.y = toPos.y;
            player.state = "moving";
            moved = true;
        } else {
            music.stop();
            ripSound.play();
            player1.state = "fallen";
            player2.state = "fallen";
            gameOver = true;
            gameOverCause = "your own stupidity";
        }

        return moved;
    }

    Image {
        anchors.fill: parent
        fillMode: Image.Tile
        source: "qrc:/assets/images/background.png"
    }

    Ai {
        id: ai
        z: 10

        onAnimalHit: {
            music.stop();
            ripSound.play();
            player1.state = "fallen";
            player2.state = "fallen";
            gameOver = true;
            gameOverCause = allAnimalData[animal.name].niceName;
        }
    }

    Timer {
        interval: 1
        running: true
        repeat: true
        onTriggered: {
            ai.update();

            if (gameOver) {
                return;
            }

            var newPos = Qt.point(-1, -1);
            var playerMoved = false;
            var repaint = false;

            // qwerty
            if (keys[Qt.Key_A]) {
                newPos = Qt.point(player1.x - player1.speed, player1.y);
                playerMoved = true;
            } else if (keys[Qt.Key_D]) {
                newPos = Qt.point(player1.x + player1.speed, player1.y);
                playerMoved = true;
            } else if (keys[Qt.Key_W]) {
                newPos = Qt.point(player1.x, player1.y - player1.speed);
                playerMoved = true;
            } else if (keys[Qt.Key_S]) {
                newPos = Qt.point(player1.x, player1.y + player1.speed);
                playerMoved = true;
            } else {
                player1.state = "";
            }

            if (playerMoved && move(player1, newPos)) {
                repaint = true;
            }

            if (!gameOver) {
                playerMoved = false;

                if (keys[Qt.Key_Left]) {
                    newPos = Qt.point(player2.x - player2.speed, player2.y);
                    playerMoved = true;
                } else if (keys[Qt.Key_Right]) {
                    newPos = Qt.point(player2.x + player2.speed, player2.y);
                    playerMoved = true;
                } else if (keys[Qt.Key_Up]) {
                    newPos = Qt.point(player2.x, player2.y - player2.speed);
                    playerMoved = true;
                } else if (keys[Qt.Key_Down]) {
                    newPos = Qt.point(player2.x, player2.y + player2.speed);
                    playerMoved = true;
                } else {
                    player2.state = "";
                }

                if (playerMoved && move(player2, newPos)) {
                    repaint = true;
                }
            }

            if (repaint) {
                skin.repaint();
            }
        }
    }

    signal keyHit()

    Keys.onPressed: {
        if (event.key === Qt.Key_Escape) {
            exitToMenuClicked();
        }

        keys[event.key] = true;
        keyHit();
    }

    Keys.onReleased: {
        keys[event.key] = false;
        keyHit();
    }

    property alias player1: player1
    property alias player2: player2

    Character {
        id: player1
        x: 100
        y: 100
        name: "player-1"
    }

    Character {
        id: player2
        x: 150
        y: 100
        name: "player-2"
    }

    Skin {
        id: skin
        z: Math.min(player1.z, player2.z) + 0.001
        anchors.fill: parent
        visible: !gameOver

        player1: gameScreen.player1
        player2: gameScreen.player2
    }

    Text {
        id: timeText
        anchors.top: parent.top
        anchors.topMargin: 10
        width: parent.width
        horizontalAlignment: Text.AlignHCenter
        font.pointSize: 20
        color: "white"
        text: "00:00:00"
        z: 1001

        Timer {
            interval: 1000
            running: !gameOver
            repeat: true
            onTriggered: {
                var now = new Date();
                var msDiff = now.getTime() - startTime.getTime();
                var d = new Date(0);
                d.setHours(0);
                d.setMilliseconds(d.getMilliseconds() + msDiff);
                parent.text = Qt.formatTime(d, "hh:mm:ss");
            }
        }
    }

    DropShadow {
        anchors.fill: timeText
        radius: 10
        samples: radius * 2
        color: "black"
        source: timeText
    }

    GameOverScreen {
        visible: gameOver
        anchors.fill: parent
        z: 1000
    }

    Component.onCompleted: {
        for (var i = 0; i < 10; ++i) {
            var grassIndex = i % 3 + 1;
            var grass = Qt.createQmlObject("import QtQuick 2.0; Image { source: 'qrc:/assets/images/grass-" + grassIndex + ".png' }", gameScreen);
            grass.x = root.getRandomInt(0, parent.width);
            grass.y = root.getRandomInt(0, parent.height);
            grass.z = 1;
        }
    }

    property var keys: []

    KeyDebugItem {
        visible: debug
        keys: [Qt.Key_W, Qt.Key_A, Qt.Key_S, Qt.Key_D]
        anchors.bottom: parent.bottom
        anchors.left: parent.left
    }

    KeyDebugItem {
        visible: debug
        keys: [Qt.Key_Up, Qt.Key_Left, Qt.Key_Down, Qt.Key_Right]
        anchors.bottom: parent.bottom
        anchors.right: parent.right
    }
}

