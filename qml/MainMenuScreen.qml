import QtQuick 2.0
import QtQuick.Layouts 1.0
import QtQuick.Controls 1.0
import QtQuick.Controls.Styles 1.0

Rectangle {
    color: "black"

    signal newGameClicked
    signal creditsClicked

//    Rectangle {
//        x: optionsLayout.x
//        y: optionsLayout.y
//        width: optionsLayout.width
//        height: optionsLayout.height
//        color: "red"
//    }

    Keys.onDownPressed: {
        if (optionsLayout.currentIndex + 1 < optionsLayout.count) {
            ++optionsLayout.currentIndex;
        }
    }

    Keys.onUpPressed: {
        if (optionsLayout.currentIndex - 1 >= 0) {
            --optionsLayout.currentIndex;
        }
    }

    function itemSelected() {
        var text = optionsLayout.model.get(optionsLayout.currentIndex).optionText;
        if (text === "New Game") {
            newGameClicked()
        } else {
            creditsClicked()
        }
    }

    Keys.onReturnPressed: itemSelected()
    Keys.onSpacePressed: itemSelected()

    ListView {
        id: optionsLayout
        width: 100
        height: 200
//        Rectangle {
//            anchors.fill: parent
//            color: "transparent"
//            border.color: "darkorange"
//        }
        anchors.centerIn: parent
        currentIndex: 0
        focus: true
//        interactive: false

        model: ListModel {
            ListElement {
                optionText: "New Game"
            }
            ListElement {
                optionText: "Credits"
            }
        }

        delegate: MenuButton {
            text: optionText
            onClicked: itemSelected()
        }
    }
}
