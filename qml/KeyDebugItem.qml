import QtQuick 2.0

Item {
    id: debugItem
    width: 99
    height: 66
    z: 1000

    property var keys: []

    Rectangle {
        id: upKey
        color: gameScreen.keys[debugItem.keys[0]] ? "red" : "grey"
        width: parent.width / 3
        height: parent.height / 2
        anchors.top: parent.top
        anchors.horizontalCenter: parent.horizontalCenter

//        Text {
//            anchors.centerIn: parent
//            text: "↑"
//        }

        Connections {
            target: gameScreen
            onKeyHit: upKey.color = gameScreen.keys[debugItem.keys[0]] ? "red" : "grey"
        }
    }

    Rectangle {
        id: leftKey
        color: gameScreen.keys[debugItem.keys[1]] ? "red" : "grey"
        width: parent.width / 3
        height: parent.height / 2
        anchors.left: parent.left
        anchors.bottom: parent.bottom

//        Text {
//            anchors.centerIn: parent
//            text: "←"
//        }

        Connections {
            target: gameScreen
            onKeyHit: leftKey.color = gameScreen.keys[debugItem.keys[1]] ? "red" : "grey"
        }
    }

    Rectangle {
        id: downKey
        color: gameScreen.keys[debugItem.keys[2]] ? "red" : "grey"
        width: parent.width / 3
        height: parent.height / 2
        anchors.bottom: parent.bottom
        anchors.horizontalCenter: parent.horizontalCenter

//        Text {
//            anchors.centerIn: parent
//            text: "↓"
//        }

        Connections {
            target: gameScreen
            onKeyHit: downKey.color = gameScreen.keys[debugItem.keys[2]] ? "red" : "grey"
        }
    }

    Rectangle {
        id: rightKey
        color: gameScreen.keys[debugItem.keys[3]] ? "red" : "grey"
        width: parent.width / 3
        height: parent.height / 2
        anchors.bottom: parent.bottom
        anchors.right: parent.right

//        Text {
//            anchors.centerIn: parent
//            text: "→"
//        }

        Connections {
            target: gameScreen
            onKeyHit: rightKey.color = gameScreen.keys[debugItem.keys[3]] ? "red" : "grey"
        }
    }
}
