import QtQuick 2.0
import QtGraphicalEffects 1.0

Rectangle {
    color: Qt.rgba(0, 0, 0, 0.5)

    Keys.onEscapePressed: gameScreen.exitToMenuClicked()
    Keys.onReturnPressed: gameScreen.exitToMenuClicked()
    Keys.onSpacePressed: gameScreen.exitToMenuClicked()

    onVisibleChanged: {
        if (visible) {
            focus = true;
        }
    }

    Text {
        id: headlineText
        text: "You were killed by..."
        color: "white"
        anchors.centerIn: parent
        font.pointSize: 14
    }

    Text {
        id: causeText
        text: (gameScreen.gameOverCause.indexOf("your") == -1 ? "a " : "") + gameScreen.gameOverCause + "!"
        color: "white"
        anchors.horizontalCenter: headlineText.horizontalCenter
        anchors.top: headlineText.bottom
        anchors.topMargin: 10
        font.pointSize: 24
    }

    DropShadow {
        anchors.fill: headlineText
        radius: 10
        samples: radius * 2
        color: "black"
        source: headlineText
    }

    DropShadow {
        anchors.fill: causeText
        radius: 10
        samples: radius * 2
        color: "black"
        source: causeText
    }
}
