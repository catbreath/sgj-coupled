import QtQuick 2.0
import QtQuick.Layouts 1.0
import QtQuick.Controls 1.0
import QtQuick.Controls.Styles 1.0

Button {
    id: button
    width: 100
    height: ListView.view.height / ListView.view.count
    anchors.horizontalCenter: parent.horizontalCenter
    focus: selected

    readonly property bool selected: ListView.isCurrentItem

    onHoveredChanged: {
        if (hovered) {
            ListView.view.currentIndex = index
        }
    }

    style: ButtonStyle {
        background: Rectangle {
            antialiasing: true
            id: rect
//            radius: 4
            transform: Rotation {
                id: rotationTransform
                origin.x: rect.width/2
//                origin.y: rect.height/2
                axis.x: 1
            }
            Connections {
                target: button
                onSelectedChanged: rotationTransform.angle = root.getRandomInt(-15, 15)
            }
            color: selected ? "#222" : "transparent"
        }

        label: Label {
            anchors.centerIn: parent
            color: "darkorange"
            text: control.text
            verticalAlignment: Text.AlignVCenter
            horizontalAlignment: Text.AlignHCenter
        }
    }
}
