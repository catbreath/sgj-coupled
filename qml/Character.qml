import QtQuick 2.0
import QtGraphicalEffects 1.0

Item {
    id: character
    width: shadowRect.width
    height: shadowRect.height
    z: state != "fallen" ? y + image.height : 1

    property string name
    property real speed: 0.5
    property point velocity: Qt.point(0, 0)
    readonly property point centre: Qt.point(x + width / 2, y + height / 2)
    readonly property rect shadowRect: Qt.rect(0, image.height * 0.8, image.width, image.height * 0.2)
    property alias image: image

    Item {
        id: shadowGuard
        x: -dropShadow.radius * 2
        y: -dropShadow.radius * 2
        width: parent.width + dropShadow.radius * 4
        height: parent.height + dropShadow.radius * 4
        visible: false

        Rectangle {
            id: shadow

            anchors.fill: parent
            anchors.margins: dropShadow.radius * 2
            radius: width * 0.5
            color: Qt.rgba(0, 0, 0, 0.15)
        }
    }

    DropShadow {
        id: dropShadow
        anchors.fill: shadowGuard
        radius: 4
        samples: radius * 2
        source: shadowGuard
        z: parent.z + 0.01
        visible: character.state != "fallen"
    }

    Image {
        id: image
        anchors.bottom: parent.bottom

        property string animationType: name.indexOf("player") == -1 ? "-moving" : "-knee"

        source: name !== "" ? "qrc:/assets/images/" + name + (sourceSwitch ? animationType : "") + ".png" : ""
        state: character.state

        transform: Rotation {
            id: rotationTransform
            angle: 0
            origin.y: height
        }

        z: parent.z + 0.02

        property bool sourceSwitch: false

        states: State {
            name: "fallen"

            PropertyChanges {
                target: rotationTransform
                angle: 90
            }
        }

        Timer {
            id: animationTimer
            interval: 300
            repeat: true
            onTriggered: image.sourceSwitch = !image.sourceSwitch
            running: character.state == "moving"

            onRunningChanged: {
                // We want them to stop with their leg down.
                if (!running) {
                    image.sourceSwitch = false;
                }
            }
        }
    }
}
