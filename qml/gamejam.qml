import QtQuick 2.0
import QtQuick.Controls 1.0
import QtMultimedia 5.0

Item {
    id: root
    width: 640
    height: 480
    focus: true

    function getRandomInt(min, max) {
        return Math.floor(Math.random() * (max - min + 1)) + min;
    }

    property Component mainMenuScreenComponent: MainMenuScreen {
        onNewGameClicked: {
            stackView.push(gameScreenComponent)
            music.play();
        }
        onCreditsClicked: stackView.push(creditsScreenComponent)
    }

    property Component creditsScreenComponent: CreditsScreen {
        onBackToMenuClicked: stackView.pop()
    }

    property Component gameScreenComponent: GameScreen {
        onExitToMenuClicked: {
            stackView.pop();
            music.stop();
        }
    }

    StackView {
        id: stackView

        onCurrentItemChanged: {
            if (currentItem) {
                currentItem.forceActiveFocus()
            }
        }
    }

    Audio {
        id: music
        source: pwd + "/audio/The Animals Of The Forest.wav"
    }

    Component.onCompleted: stackView.push(mainMenuScreenComponent)
}
