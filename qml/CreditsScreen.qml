import QtQuick 2.0
import QtQuick.Controls 1.0
import QtQuick.Layouts 1.0

Rectangle {
    color: "black"
    signal backToMenuClicked

    Keys.onSpacePressed: backToMenuClicked()
    Keys.onReturnPressed: backToMenuClicked()
    Keys.onEscapePressed: backToMenuClicked()

    readonly property real textZ: 1000

    MouseArea {
        anchors.fill: parent
        onClicked: backToMenuClicked()
    }

    Ai {
        collisionChecks: false
        Timer {
            interval: 1
            running: true
            repeat: true
            onTriggered: parent.update()
        }
    }

    ColumnLayout {
        anchors.centerIn: parent
        spacing: 10
        z: textZ

        ColumnLayout {
            spacing: 0
            anchors.horizontalCenter: parent.horizontalCenter

            Label {
                text: "Programming"
                color: "yellow"
                font.pointSize: 12
                anchors.horizontalCenter: parent.horizontalCenter
            }
            Label {
                text: "Mitch Curtis"
                color: "yellow"
                font.pointSize: 10
                anchors.horizontalCenter: parent.horizontalCenter
            }
        }

        ColumnLayout {
            spacing: 0
            anchors.horizontalCenter: parent.horizontalCenter

            Label {
                text: "Graphics"
                color: "yellow"
                font.pointSize: 12
                anchors.horizontalCenter: parent.horizontalCenter
            }
            Label {
                text: "Triva Linda"
                color: "yellow"
                font.pointSize: 10
                anchors.horizontalCenter: parent.horizontalCenter
            }
        }

        ColumnLayout {
            spacing: 0
            anchors.horizontalCenter: parent.horizontalCenter

            Label {
                text: "Audio"
                color: "yellow"
                font.pointSize: 12
                anchors.horizontalCenter: parent.horizontalCenter
            }
            Label {
                text: "Ådne Nilsen"
                color: "yellow"
                font.pointSize: 10
                anchors.horizontalCenter: parent.horizontalCenter
            }
        }
    }
}
