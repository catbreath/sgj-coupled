import QtQuick 2.0

Item {
    id: aiItem

    property var allAnimals: ["baby-fox", "mama-fox", "deer", "beaver", "rabbit", "cat"]

    property var allAnimalData: {
        "baby-fox": {
            speed: 0.9,
            niceName: "baby fox"
        },
        "mama-fox": {
            speed: 1.0,
            niceName: "mama fox"
        },
        "deer": {
            speed: 1.1,
            niceName: "deer"
        },
        "beaver": {
            speed: 0.4,
            niceName: "beaver"
        },
        "rabbit": {
            speed: 1.2,
            niceName: "rabbit"
        },
        "cat": {
            speed: 0.8,
            niceName: "cat"
        }
    }

    property var animals: []
    readonly property int max: root.height / 50
    property bool collisionChecks: true

    signal animalHit(var player, var animal)

    Timer {
        id: releaseTimer
        running: true
        interval: 1000
    }

    function checkPointCollision(character, x, y) {
        var hit = aiItem.childAt(x, y);
        if (hit && hit.name.indexOf("player") === -1) {
            return hit;
        }

        return null;
    }

    function isCollision(ch) {
        var xPointsToCheck = 4;
        var yPointsToCheck = 4;

        var hit = null;
        for (var y = 0; y <= yPointsToCheck && !hit; ++y) {
            for (var x = 0; x <= xPointsToCheck && !hit; ++x) {
                hit = checkPointCollision(ch,
                    ch.x + ((ch.width / xPointsToCheck) * x),
                    ch.y + ((ch.height / yPointsToCheck) * y))
            }
        }
        return hit;
    }

    function update() {
        if (animals.length < max && !releaseTimer.running) {
            var animal = Qt.createQmlObject("import QtQuick 2.0; Character {}", aiItem);
            animal.x = aiItem.parent.width;
            animal.y = root.getRandomInt(0, aiItem.parent.height - animal.height);
            animal.name = allAnimals[root.getRandomInt(0, allAnimals.length - 1)];
            animal.speed = allAnimalData[animal.name].speed;
            animals.push(animal);

            releaseTimer.interval = root.getRandomInt(0, 1000);
            releaseTimer.start();
        }

        if (collisionChecks && !gameScreen.gameOver) {
            var hitAnimal = isCollision(gameScreen.player1);
            if (hitAnimal) {
                animalHit(gameScreen.player1, hitAnimal)
            } else {
                hitAnimal = isCollision(gameScreen.player2);
                if (hitAnimal) {
                    animalHit(gameScreen.player2, hitAnimal)
                }
            }
        }

        for (var i = 0; i < animals.length; ) {
            animal = animals[i];
            animal.state = "moving";
            animal.x -= animal.speed;

            if (animal.x + animal.width < 0) {
                animals.splice(i, 1);
                animal.destroy();
            } else {
                ++i;
            }
        }
    }
}
