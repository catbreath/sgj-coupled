#include "Skin.h"

#include <QPainter>

Skin::Skin(QQuickItem *parent) :
    QQuickPaintedItem(parent),
    mPlayer1(0),
    mPlayer2(0),
    mSkinTransparencyMinimum(0.15),
    mSkinStartY(35),
    mSkinHeight(27),
    mDistanceThreshold(150),
    mColor(QColor(239, 208, 207, 255))
{
}

void Skin::paint(QPainter *painter)
{
    qreal opacity = qMin(1.0, 1.0 - (distanceBetweenPlayers() / mDistanceThreshold) + mSkinTransparencyMinimum);
    mColor.setAlpha(opacity * 255);

    const QQuickItem *player1Image = mPlayer1->property("image").value<QQuickItem*>();
    const QQuickItem *player2Image = mPlayer2->property("image").value<QQuickItem*>();

    painter->setRenderHint(QPainter::Antialiasing, true);

    QPainterPath path;
    path.moveTo(mPlayer1->x() + player1Image->x() + player1Image->width(), mPlayer1->y() + player1Image->y() + mSkinStartY);
    path.lineTo(mPlayer1->x() + player1Image->x() + player1Image->width(), mPlayer1->y() + player1Image->y() + mSkinStartY + mSkinHeight);
    path.lineTo(mPlayer2->x() + player2Image->x(), mPlayer2->y() + player2Image->y() + mSkinStartY + mSkinHeight);
    path.lineTo(mPlayer2->x() + player2Image->x(), mPlayer2->y() + player2Image->y() + mSkinStartY);
    path.closeSubpath();
    painter->fillPath(path, QBrush(mColor));
}

void Skin::repaint()
{
    update();
}

qreal Skin::distance(qreal x1, qreal y1, qreal w1, qreal h1, qreal x2, qreal y2, qreal w2, qreal h2) const
{
    return sqrt(pow((x2 + w2 / 2) - (x1 + w1 / 2), 2) + pow((y2 + h2 / 2) - (y1 + h1 / 2), 2));
}

qreal Skin::distanceBetweenPlayers() const
{
    return distance(mPlayer1->x(), mPlayer1->y(), mPlayer1->width(), mPlayer1->height(),
        mPlayer2->x(), mPlayer2->y(), mPlayer2->width(), mPlayer2->height());
}

qreal Skin::distanceThreshold() const
{
    return mDistanceThreshold;
}
