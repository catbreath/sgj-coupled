folder_01.source = qml
folder_01.target = qml
DEPLOYMENTFOLDERS = folder_01

sounds.source = assets/audio
CONFIG(debug, debug|release) {
    sounds.target = debug
} else {
    sounds.target = release
}
DEPLOYMENTFOLDERS = sounds

# Additional import path used to resolve QML modules in Creator's code model
QML_IMPORT_PATH =

# The .cpp file which was generated for your project. Feel free to hack it.
SOURCES += main.cpp \
    Skin.cpp

OTHER_FILES += qml/*

# Installation path
# target.path =

# Please do not modify the following two lines. Required for deployment.
include(qtquick2applicationviewer/qtquick2applicationviewer.pri)
qtcAddDeployment()

RESOURCES += \
    resources.qrc

HEADERS += \
    Skin.h
