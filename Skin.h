#ifndef SKIN_H
#define SKIN_H

#include <QQuickPaintedItem>

class Skin : public QQuickPaintedItem
{
    Q_OBJECT
    Q_PROPERTY(QQuickItem *player1 MEMBER mPlayer1)
    Q_PROPERTY(QQuickItem *player2 MEMBER mPlayer2)
    Q_PROPERTY(qreal distanceThreshold READ distanceThreshold)
public:
    explicit Skin(QQuickItem *parent = 0);

    void paint(QPainter *painter);
    Q_INVOKABLE void repaint();

    Q_INVOKABLE qreal distance(qreal x1, qreal y1, qreal w1, qreal h1, qreal x2, qreal y2, qreal w2, qreal h2) const;
    qreal distanceThreshold() const;
    Q_INVOKABLE qreal distanceBetweenPlayers() const;
signals:

public slots:
private:
    QQuickItem *mPlayer1;
    QQuickItem *mPlayer2;
    const qreal mSkinTransparencyMinimum;
    const qreal mSkinStartY;
    const qreal mSkinHeight;
    const qreal mDistanceThreshold;
    QColor mColor;
};

#endif // SKIN_H
